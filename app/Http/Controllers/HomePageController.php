<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\User;

class HomePageController extends Controller {
    public function index() {
        $images = \File::allFiles(public_path('images'));
        return View('welcome')->with(array('images'=>$images));
    }
}
