<script>
$(document).ready(function() {

    'use strict';

    $('#dataTable1').DataTable();

    var exRowTable = $('#exRowTable').DataTable({
        responsive: true,
        'fnDrawCallback': function(oSettings) {
            $('#exRowTable_paginate ul').addClass('pagination-active-success');
        },
        'ajax': 'ajax/objects.txt',
        'columns': [{
            'class': 'details-control',
            'orderable': false,
            'data': null,
            'defaultContent': ''
        },
            { 'data': 'name' },
            { 'data': 'position' },
            { 'data': 'office' },
            { 'data': 'start_date'},
            { 'data': 'salary' }
        ],
        'order': [[1, 'asc']]
    });

    // Add event listener for opening and closing details
    $('#exRowTable tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = exRowTable.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    });

    function format (d) {
        // `d` is the original data object for the row
        return '<h4>'+d.name+'<small>'+d.position+'</small></h4>'+
            '<p class="nomargin">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>';
    }

    // Select2
    $('select').select2({ minimumResultsForSearch: Infinity });

});
</script>
