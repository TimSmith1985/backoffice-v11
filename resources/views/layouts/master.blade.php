<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('includes.header')

<body>

<header>
    <div class="headerpanel">

        <div class="logopanel">
            <h2><a href="index.html">BACK OFFICE</a></h2>
        </div><!-- logopanel -->

        <div class="headerbar">

            <a id="menuToggle" class="menutoggle"><i class="fa fa-bars"></i></a>

            @include('includes.navigation')

        </div><!-- headerbar -->
    </div><!-- header-->
</header>

<div id="page-wrapper">

    {{--        @yield('breadcrumbs')--}}
    @yield('content')
</div>

</div>


@include('includes.footer')

</body>
</html>
