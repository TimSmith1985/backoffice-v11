@extends('layouts.master')

@section('content')


    <div class="mainpanel">
        <div class="contentpanel">

            <ol class="breadcrumb breadcrumb-quirk">
                <li><a href="/dashboard"><i class="fa fa-home mr5"></i> Dashboard</a></li>
            </ol>
            <hr class="darken">



        </div>
    </div>

@stop

@section('js')
    @parent
    <script src="../lib/jquery/jquery.js"></script>
    <script src="../lib/jquery-ui/jquery-ui.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
    <script src="../lib/jquery-toggles/toggles.js"></script>

    <script src="../lib/flot/jquery.flot.js"></script>
    <script src="../lib/flot/jquery.flot.resize.js"></script>
    <script src="../lib/flot/jquery.flot.symbol.js"></script>
    <script src="../lib/flot/jquery.flot.crosshair.js"></script>
    <script src="../lib/flot/jquery.flot.categories.js"></script>
    <script src="../lib/flot/jquery.flot.pie.js"></script>

    <script src="../lib/flot-spline/jquery.flot.spline.js"></script>

    <script src="../lib/morrisjs/morris.js"></script>
    <script src="../lib/raphael/raphael.js"></script>

    <script src="../lib/jquery-sparkline/jquery.sparkline.js"></script>


    <script src="../js/charts.js"></script>

@stop
