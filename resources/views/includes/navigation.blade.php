<section>

    <div class="leftpanel">
        <div class="leftpanelinner">

            <ul class="nav nav-tabs nav-justified nav-sidebar">
                <li class="tooltips active" data-toggle="tooltip" title="Main Menu"><a data-toggle="tab" data-target="#mainmenu"><i class="tooltips fa fa-ellipsis-h"></i></a></li>
                {{--                <li class="tooltips unread" data-toggle="tooltip" title="Check Mail"><a data-toggle="tab" data-target="#emailmenu"><i class="tooltips fa fa-envelope"></i></a></li>--}}
                {{--                <li class="tooltips" data-toggle="tooltip" title="Contacts"><a data-toggle="tab" data-target="#contactmenu"><i class="fa fa-user"></i></a></li>--}}
                {{--                <li class="tooltips" data-toggle="tooltip" title="Settings"><a data-toggle="tab" data-target="#settings"><i class="fa fa-cog"></i></a></li>--}}
                <li class="tooltips" data-toggle="tooltip" title="Log Out"><a href="signin.html"><i class="fa fa-sign-out"></i></a></li>
            </ul>

            <div class="tab-content">

                <!-- ################# MAIN MENU ################### -->

                <div class="tab-pane active" id="mainmenu">
                    <ul class="nav nav-pills nav-stacked nav-quirk">
                        <li><a href="/dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
                    </ul>

                    <h5 class="sidebar-title">Main Menu</h5>
                    <ul class="nav nav-pills nav-stacked nav-quirk">
                        <li class="nav-parent">
                            <a href=""><i class="fa fa-exclamation-triangle"></i> <span style="color: red">Breakdown</span></a>
                            <ul class="children">
                                <li><a href="/company/breakdown">Add Breakdown</a></li>
                                <li><a href="/company/outstanding-breakdowns">Outstanding Breakdown</a></li>
                            </ul>
                        </li>
                        <li class="nav-parent">
                            <a href=""><i class="fa fa-check-square"></i> <span>Businesses</span></a>
                            <ul class="children">
                                <li><a href="/company/create">Add Business</a></li>
                                <li><a href="/company-list">List Businesses</a></li>
                                <li><a href="/company-role-create">Add Businesses Role</a></li>
                                <li><a href="/company-role-list">View Businesses Role</a></li>
                                <li><a href="/completed-service">View Completed Services</a></li>

                            </ul>
                        </li>
                        <li class="nav-parent">
                            <a href=""><i class="fa fa-check-square"></i> <span>Categories</span></a>
                            <ul class="children">
                                <li><a href="/company/schools">Schools</a></li>
                                <li><a href="/company/commercial">Commercial</a></li>
                                <li><a href="/company/sanitary">sanitary</a></li>
                                <li><a href="/company/leisure">Leisure Centers</a></li>
                                <li><a href="/company/education">Further Education</a></li>
                                <li><a href="/company/none">Non Assigned</a></li>

                            </ul>
                        </li>
                        <li class="nav-parent"><a href=""><i class="fa fa-suitcase"></i> <span>Contracts</span></a>
                            <ul class="children">
                                <li><a href="/all-contracts">All Contracts</a></li>
                                <li><a href="/expiring-contracts">Expiring Contracts</a></li>
                                <li><a href="/contract-create">Add Contract Type</a></li>
                                <li><a href="/contracts-list">View Contract Type</a></li>

                            </ul>
                        </li>
                        <li class="nav-parent"><a href=""><i class="fa fa-th-list"></i> <span>Products</span></a>
                            <ul class="children">
                                <li><a href="/product/create">Add Product</a></li>
                                <li><a href="/product-list">View Products</a></li>
                                <li><a href="/product-category-create">Add Product Categories</a></li>
                                <li><a href="/product-category-list">View Product Categories</a></li>
                            </ul>
                        </li>
                        <li class="nav-parent"><a href=""><i class="fa fa-file-text"></i> <span>Service List</span></a>
                            <ul class="children">
                                <li><a href="/service-list">View All Service List</a></li>
                                <li><a href="/service-list/zone-a">Zone A Service List</a></li>
                                <li><a href="/service-list/zone-b">Zone B Service List</a></li>
                            </ul>
                        </li>
                        <li class="nav-parent"><a href=""><i class="fa fa-user"></i> <span>Engineers</span></a>
                            <ul class="children">
                                <li><a href="/eng-overview">Engineer overview</a></li>
                                <li><a href="/company-list/unassigned">Unassigned Companies</a></li>
                                <li><a href="/company-list/zone-a">Zone A Companies</a></li>
                                <li><a href="/company-list/zone-b">Zone B Companies</a></li>
                            </ul>
                        </li>
                        <li class="nav-parent"><a href=""><i class="fa fa-gears"></i> <span>Support</span></a>
                            <ul class="children">
                                <li><a href="/bug">Report a bug</a></li>
                                <li><a href="/feature">Request a feature or upgrade</a></li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- tab-pane -->

            </div>
        </div>
    </div>
</section>

