<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--<link rel="shortcut icon" href="../images/favicon.png" type="image/png">-->

    <title>BACK OFFICE</title>

    <link rel="stylesheet" href="../lib/Hover/hover.css">
    <link rel="stylesheet" href="../lib/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="../lib/weather-icons/css/weather-icons.css">
    <link rel="stylesheet" href="../lib/ionicons/css/ionicons.css">
    <link rel="stylesheet" href="../lib/jquery-toggles/toggles-full.css">
    <link rel="stylesheet" href="../lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css">
    <link rel="stylesheet" href="../lib/select2/select2.css">
    <link rel="stylesheet" href="../lib/morrisjs/morris.css">
    <link rel="stylesheet" href="../lib/jquery.steps/jquery.steps.css">
    {{--    <link rel="stylesheet" href="sweetalert2.min.css">--}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    {{--    <script src="sweetalert2.all.min.js"></script>--}}
<!-- Optional: include a polyfill for ES6 Promises for IE11 -->
    {{--    <script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>--}}



    <link rel="stylesheet" href="../css/quirk.css">

    <script src="../lib/modernizr/modernizr.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../lib/html5shiv/html5shiv.js"></script>
    <script src="../lib/respond/respond.src.js"></script>
    <![endif]-->
</head>
