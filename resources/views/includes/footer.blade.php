@section('js')
    @parent

    <script src="../lib/jquery/jquery.js"></script>
    <script src="../lib/jquery-ui/jquery-ui.js"></script>
    <script src="../lib/bootstrap/js/bootstrap.js"></script>
    <script src="../lib/jquery-toggles/toggles.js"></script>
    <script src="../lib/datatables/jquery.dataTables.js"></script>
    <script src="../lib/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.js"></script>
    <script src="../lib/select2/select2.js"></script>

    <script src="../lib/morrisjs/morris.js"></script>
    <script src="../lib/raphael/raphael.js"></script>

    <script src="../lib/flot/jquery.flot.js"></script>
    <script src="../lib/flot/jquery.flot.resize.js"></script>
    <script src="../lib/flot-spline/jquery.flot.spline.js"></script>
    <script src="../lib/jquery-toggles/toggles.js"></script>

    <script src="../lib/jquery-knob/jquery.knob.js"></script>

    <script src="../js/quirk.js"></script>
    <script src="../js/dashboard.js"></script>
    <script>
        function notificationBar(type, message, url, title, showLoading = false, confirm = false, site = null, element = null, timer = null) {

            // TODO : THIS
            // defaultconfig = {
            //
            // }
            //
            // config = {
            //     title: 'some title',
            //     description: 'some description',
            //     showloading:
            //     confirm:
            // }
            //
            // _.each(config, function(c){
            //     config.key = value
            // })

            let btnColor = '';
            let confirmText = 'Ok';
            if (typeof title == 'undefined') {
                if (type == 'error') {
                    title = 'Something went wrong!'
                    // btnColor = '#ff0000'
                }
                if (type == 'success') {
                    title = 'Done!'
                    //btnColor = '#00ff00'
                }
                if (type == 'delete') {
                    title = 'Delete?!'
                    confirmText = 'Yes, delete it!';
                    // type = 'warning'
                    //btnColor = '#00ff00'
                }
            }
            // if (type == 'confirmation') {
            //     type = 'warning';
            //     title = 'Are you sure?';
            //     confirmText = 'Yes i\'m sure!';
            //     // type = 'warning'
            //     //btnColor = '#00ff00'
            // }


            Swal.fire({
                title: title,
                text: message,
                timer: timer,
                type: type === 'delete' ? 'warning' : type,
                confirmButtonText: confirmText,
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
                cancelButtonColor: 'red',
                confirmButtonColor: 'green',
                showCancelButton: confirm,
                showConfirmButton: timer != null ? false : true,
                onBeforeOpen: () => {
                    if(showLoading)
                        Swal.showLoading()
                }
            }).then(function (result) {

                if (result.value && url !== undefined && type !== 'delete') {
                    setTimeout(function(){
                        window.location.href = url;
                    }, 500);
                }

                if(result.value && url == 'reload-page'){
                    location.reload()
                }

                //TODO:: check for delete type
                // do the ajax request using URL as the URL for the ajax
                if(result.value && type === 'delete' && url !== undefined){
                    loading('#button-edits', 'Deleting', 'We are deleting the item, please wait.');
                    $.ajax({
                        type: "DELETE",
                        url: url,
                        success: function (data) {
                            if(url.indexOf('pages') > -1){
                                getPages(site, 'delete');
                            }

                            if(url.indexOf('table-plans') > -1){
                                console.log(element)
                                $('.grid-snap[data-id="'+element+'"]').remove();
                            }

                            if(url.indexOf('pages') === -1 && url.indexOf('table-plans') === -1) {
                                table.ajax.reload();
                                notificationBar('success', 'Item Deleted', undefined, 'Delete');
                            }

                        },
                        fail: function (data) {
                            notificationBar('error', 'The selected item has not been deleted, Please try again.');
                        },
                        error: function (data) {
                            notificationBar(data.responseJSON.status, data.responseJSON.message)
                        }
                    })
                }

                return false;

            })
        }
    </script>

    @stack('scripts')

@stop

@yield('js')
